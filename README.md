# ZUK-Mobile


## Describtion
This project was created within the university framework and the name ZUK signed together with Ukraine. 

The aim is to enable everyone during the war period in Ukraine to help Ukrainians who arrived in Germany. The help is in all forms.
Any person can create a post describing what he would like to help with, then the Admins of the platform receive the post in a dashboard (admin area) and have to check it and publish if it respects the rules. Each person interested in a post can click on it and write a request message. The Admins have to study the request and put him in contact with the author of the post.


## Backend
The Backend is an Spring boot project.

Repos Link: https://gitlab.com/Baudouinken/zuk-api


## Deploy
The App is doplyed on a linux root-server with gitlab-runners through a gitlab.yml file
