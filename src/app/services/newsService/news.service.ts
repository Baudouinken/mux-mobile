import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { News } from 'src/app/models/News';

@Injectable({
  providedIn: 'root',
})
export class NewsService {
  readonly newsUrl: string;
  readonly headers: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders({ 'content-type': 'application/json' });
    this.newsUrl =  `${environment.server}/news`;
  }

  // GET ONE
  getById<News>(id: string): Observable<News> {
    return this.httpClient.get<News>(`${this.newsUrl}/${id}`, {
      headers: this.headers,
    });
  }

  // GET ALL
  getAll(): Observable<News[]> {
    return this.httpClient.get<News[]>(this.newsUrl, {
      headers: this.headers,
    });
  }

  // POST
  add(news: News): Observable<News> {
    return this.httpClient.post<News>(this.newsUrl, news);
  }

  // PUT
  update(news: News): Observable<News> {
    return this.httpClient.put<News>(this.newsUrl, news, {
      headers: this.headers,
    });
  }

  // DELETE
  delete(id: string) {
    return this.httpClient.delete(`${this.newsUrl}/${id}`, {
      responseType: 'text',
    });
  }
}
