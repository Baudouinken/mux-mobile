import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { LocalStorageService } from 'ngx-webstorage';

import { environment } from 'src/environments/environment';
import { Person } from 'src/app/models/Person';

interface Login {
  username: string;
  password: string;
}

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  readonly authenticationUrl: string;
  readonly headers: HttpHeaders;
  readonly userkey = 'zuk-user';
  readonly tokenkey = 'zuk-token';
  readonly refreshtokenkey = 'zuk-refreshtoken';

  constructor(
    private http: HttpClient,
    private router: Router,
    private localStorageService: LocalStorageService
  ) {
    this.authenticationUrl = `${environment.server}/api`;
    this.headers = new HttpHeaders({ 'content-type': 'application/json' });
  }

  public saveUser(user: any): void {
    this.localStorageService.clear(this.userkey);
    this.localStorageService.store(this.userkey, user);
  }

  clear() {
    this.localStorageService.clear(this.tokenkey);
    this.localStorageService.clear(this.userkey);
    this.localStorageService.clear(this.refreshtokenkey);
  }

  public currentUser() {
    return this.localStorageService.retrieve(this.userkey);
  }

  public saveToken(token: string) {
    this.localStorageService.clear(this.tokenkey);
    this.localStorageService.store(this.tokenkey, token);

    const user = this.currentUser();
    if (user.userUuid) {
      this.saveUser({ user, accessToken: token });
    }
  }

  public getToken(): string | null {
    return this.localStorageService.retrieve(this.tokenkey);
  }

  public saveRefreshToken(token: string): void {
    this.localStorageService.clear(this.refreshtokenkey);
    this.localStorageService.store(this.refreshtokenkey, token);
  }

  public getRefreshToken(): string | null {
    return this.localStorageService.retrieve(this.refreshtokenkey);
  }

  login(credentials: Login): Observable<any> {
    return this.http
      .post(
        `${this.authenticationUrl}/`,
        {
          username: credentials.username,
          password: credentials.password,
        },
        { headers: this.headers }
      )
      .pipe(
        map((user) => {
          // Store the user details and token to keep user logged in between page refreshes
          this.saveUser(user);
          return user;
        })
      );
  }

  register(user: Person): Observable<any> {
    return this.http.post(
      `${this.authenticationUrl}/`,
      {
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email,
        phone: user.phone,
        gender: user.gender,
        password: user.password,
        role: user.role,
        dob: user.dob,
        nationality: user.nationality,
        address: user.address,
      },
      { headers: this.headers }
    );
  }

  refreshToken(token: string) {
    return this.http.post(
      `${this.authenticationUrl}/`,
      {
        refreshToken: token,
      },
      { headers: this.headers }
    );
  }

  logOut() {
    this.clear();
    this.router.navigate(['login']);
    //location.reload();
  }
}
