import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { Category } from 'src/app/models/Category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  readonly categoryUrl: string;
  readonly headers: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.categoryUrl = `${environment.server}/categories`;
    this.headers = new HttpHeaders({ 'content-type': 'application/json' });
  }

  // GET ONE
  getById<Category>(id: string): Observable<Category> {
    return this.httpClient.get<Category>(`${this.categoryUrl}/${id}`, {
      headers: this.headers,
    });
  }

  getByName<Category>(name: string): Observable<Category> {
    return this.httpClient.get<Category>(`${this.categoryUrl}/${name}`, {
      headers: this.headers,
    });
  }

  // GET ALL
  getAll(): Observable<Category[]> {
    return this.httpClient.get<Category[]>(this.categoryUrl, {
      headers: this.headers,
    });
  }

  // POST
  add(category: Category): Observable<Category> {
    return this.httpClient.post<Category>(
      this.categoryUrl,
      category
    );
  }

  // PUT
  update(category: Category): Observable<Category> {
    return this.httpClient.put<Category>(
      this.categoryUrl,
      category,
      { headers: this.headers }
    );
  }

  // DELETE
  delete(id: string) {
    return this.httpClient.delete(`${this.categoryUrl}/${id}`, {
      responseType: 'text',
    });
  }
}
