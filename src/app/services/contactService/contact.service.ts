import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { Contact } from 'src/app/models/Contact';

@Injectable({
  providedIn: 'root',
})
export class ContactService {
  readonly contactUrl: string;
  readonly headers: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.contactUrl = `${environment.server}/contacts`;
    this.headers = new HttpHeaders({ 'content-type': 'application/json' });
  }

  // GET ONE
  getById<Contact>(id: string): Observable<Contact> {
    return this.httpClient.get<Contact>(`${this.contactUrl}/${id}`, {
      headers: this.headers,
    });
  }

  // GET ALL
  getAll(): Observable<Contact[]> {
    return this.httpClient.get<Contact[]>(this.contactUrl, {
      headers: this.headers,
    });
  }

  // POST
  add(contact: Contact): Observable<Contact> {
    return this.httpClient.post<Contact>(this.contactUrl, contact);
  }

  // PUT
  update(contact: Contact): Observable<Contact> {
    return this.httpClient.put<Contact>(this.contactUrl, contact, {
      headers: this.headers,
    });
  }

  // DELETE
  delete(id: string) {
    return this.httpClient.delete(`${this.contactUrl}/${id}`, {
      responseType: 'text',
    });
  }
}
