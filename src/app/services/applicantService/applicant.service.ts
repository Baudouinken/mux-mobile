import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { Applicant } from 'src/app/models/Applicant';

@Injectable({
  providedIn: 'root',
})
export class ApplicantService {
  readonly applicantUrl: string;
  readonly headers: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.applicantUrl = `${environment.server}/applicants`;
    this.headers = new HttpHeaders({ 'content-type': 'application/json' });
  }

  // GET ONE
  getById<Applicant>(id: string): Observable<Applicant> {
    return this.httpClient.get<Applicant>(`${this.applicantUrl}/${id}`, {
      headers: this.headers,
    });
  }

  // GET ALL
  getAll(): Observable<Applicant[]> {
    return this.httpClient.get<Applicant[]>(this.applicantUrl, {
      headers: this.headers,
    });
  }

  // GET ALL BY Applicant
  getByApplicantId<Applicant>(applicantId: string): Observable<Applicant[]> {
    return this.httpClient.get<Applicant[]>(
      `${this.applicantUrl}/${applicantId}`,
      {
        headers: this.headers,
      }
    );
  }

  getByApplicantEmail<Applicant>(
    applicantEmail: string
  ): Observable<Applicant[]> {
    return this.httpClient.get<Applicant[]>(
      `${this.applicantUrl}/${applicantEmail}`,
      {
        headers: this.headers,
      }
    );
  }

  getByAnnouncementId<Applicant>(
    announcementId: string
  ): Observable<Applicant[]> {
    return this.httpClient.get<Applicant[]>(
      `${this.applicantUrl}/${announcementId}`,
      {
        headers: this.headers,
      }
    );
  }

  // POST
  add(applicant: Applicant): Observable<Applicant> {
    return this.httpClient.post<Applicant>(this.applicantUrl, applicant);
  }

  // PUT
  update(applicant: Applicant): Observable<Applicant> {
    return this.httpClient.put<Applicant>(this.applicantUrl, applicant, {
      headers: this.headers,
    });
  }

  // DELETE
  delete(id: string) {
    return this.httpClient.delete(`${this.applicantUrl}/${id}`, {
      responseType: 'text',
    });
  }
}
