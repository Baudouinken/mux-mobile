import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { Announcement } from 'src/app/models/Announcement';

@Injectable({
  providedIn: 'root',
})
export class AnnouncementService {
  readonly announcementUrl: string;
  readonly headers: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.announcementUrl = `${environment.server}/announcements`;
    this.headers = new HttpHeaders({ 'content-type': 'application/json' });
  }

  // GET ONE
  getById<Announcement>(id: string): Observable<Announcement> {
    return this.httpClient.get<Announcement>(`${this.announcementUrl}/${id}`, {
      headers: this.headers,
    });
  }

  // GET ALL
  getAll(): Observable<Announcement[]> {
    return this.httpClient.get<Announcement[]>(this.announcementUrl, {
      headers: this.headers,
    });
  }

  // GET ALL BY CATEGORY
  getByCategoryId(categoryId: string): Observable<Announcement[]> {
    return this.httpClient.get<Announcement[]>(
      this.announcementUrl,
      {
        headers: this.headers,
        params: {
          categoryId: `${categoryId}`
        }
      }
    );
  }

  // POST
  add(
    announcement: Announcement,
    categoryId: string
  ): Observable<Announcement> {
    return this.httpClient.post<Announcement>(
      `${this.announcementUrl}/${categoryId}`,
      announcement
    );
  }

  // PUT
  update(announcement: Announcement): Observable<Announcement> {
    return this.httpClient.put<Announcement>(
      this.announcementUrl,
      announcement,
      { headers: this.headers }
    );
  }

  // DELETE
  delete(id: string) {
    return this.httpClient.delete(`${this.announcementUrl}/${id}`, {
      responseType: 'text',
    });
  }
}
