import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { Person } from 'src/app/models/Person';
import { Helper } from 'src/app/models/Helper';
import { Manager } from 'src/app/models/Manager';
import { Seeker } from 'src/app/models/Seeker';

import { ROLETYPE } from 'src/app/models/Status';

@Injectable({
  providedIn: 'root',
})
export class PersonService {
  private readonly helperUrl: string;
  private readonly seekerUrl: string;
  private readonly managerUrl: string;
  private readonly headers: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.helperUrl = `${environment.server}/helpers`;
    this.seekerUrl = `${environment.server}/seekers`;
    this.managerUrl = `${environment.server}/managers`;
    this.headers = new HttpHeaders({
      'content-type': 'application/json',
    });
  }

  // GET ONE
  getById(
    type: typeof Person,
    id: string
  ): Observable<Manager> | Observable<Helper> | Observable<Seeker> {
    if (type.name.toLowerCase() === ROLETYPE.helper.toLowerCase()) {
      return this.httpClient.get<Helper>(`${this.helperUrl}/${id}`, {
        headers: this.headers,
      });
    } else if (type.name.toLowerCase() === ROLETYPE.seeker.toLowerCase()) {
      return this.httpClient.get<Seeker>(`${this.seekerUrl}/${id}`, {
        headers: this.headers,
      });
    }
    return this.httpClient.get<Manager>(`${this.managerUrl}/${id}`, {
      headers: this.headers,
    });
  }

  // GET ALL
  getAll(
    type: typeof Person
  ): Observable<Manager[]> | Observable<Helper[]> | Observable<Seeker[]> {
    if (type.name.toLowerCase() === ROLETYPE.helper.toLowerCase()) {
      return this.httpClient.get<Helper[]>(this.helperUrl, {
        headers: this.headers,
      });
    } else if (type.name.toLowerCase() === ROLETYPE.seeker.toLowerCase()) {
      return this.httpClient.get<Seeker[]>(this.seekerUrl, {
        headers: this.headers,
      });
    }
    return this.httpClient.get<Manager[]>(this.managerUrl, {
      headers: this.headers,
    });
  }

  // POST
  add<T extends Person>(
    type: typeof Person,
    person: T
  ): Observable<Manager> | Observable<Helper> | Observable<Seeker> {
    if (type.name.toLowerCase() === ROLETYPE.helper.toLowerCase()) {
      return this.httpClient.post<Helper>(this.helperUrl, person, {
        headers: this.headers,
      });
    } else if (type.name.toLowerCase() === ROLETYPE.seeker.toLowerCase()) {
      return this.httpClient.post<Seeker>(this.seekerUrl, person, {
        headers: this.headers,
      });
    }
    return this.httpClient.post<Manager>(this.managerUrl, person, {
      headers: this.headers,
    });
  }

  // PUT
  update<T extends Person>(
    type: typeof Person,
    person: T
  ): Observable<Manager> | Observable<Helper> | Observable<Seeker> {
    if (type.name.toLowerCase() === ROLETYPE.helper.toLowerCase()) {
      return this.httpClient.put<Helper>(this.helperUrl, person, {
        headers: this.headers,
      });
    } else if (type.name.toLowerCase() === ROLETYPE.seeker.toLowerCase()) {
      return this.httpClient.put<Seeker>(this.seekerUrl, person, {
        headers: this.headers,
      });
    }
    return this.httpClient.put<Manager>(this.managerUrl, person, {
      headers: this.headers,
    });
  }

  // DELETE
  delete(type: typeof Person, id: string): Observable<string> {
    if (type.name.toLowerCase() === ROLETYPE.helper.toLowerCase()) {
      return this.httpClient.delete(`${this.helperUrl}/${id}`, {
        responseType: 'text',
      });
    } else if (type.name.toLowerCase() === ROLETYPE.seeker.toLowerCase()) {
      return this.httpClient.delete(`${this.seekerUrl}/${id}`, {
        responseType: 'text',
      });
    }
    return this.httpClient.delete(`${this.managerUrl}/${id}`, {
      responseType: 'text',
    });
  }
}
