/**Contains methods that could be called anywhere in the app */
export class Helper {
  public static capitalizeFirstLetter(value: string): string {
    return value.charAt(0).toUpperCase() + value.slice(1);
  }

  public static getCurrentDateAsString(): string {
    return new Date().toISOString().split('T')[0].split('-').reverse().join('-').toString();
  }

  public static dateToString(date: Date): string {
    return new Date(date).toISOString().split('T')[0].split('-').reverse().join('-').toString();
  }

  public static extractWord(text: string, total: number): string {
    return `${text.split(' ').slice(0, total).join(' ').toString()} ...`;
  }

  public static extractCharacter(text: string, total: number): string {
    return `${text.slice(0, total).toString()} ...`;
  }
}
