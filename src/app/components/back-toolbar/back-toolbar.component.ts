import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-back-toolbar',
  templateUrl: './back-toolbar.component.html',
  styleUrls: ['./back-toolbar.component.scss'],
})
export class BackToolbarComponent implements OnInit {

  @Input()
  title: string = null;

  constructor(private location: Location) {
    this.title = '';
   }

  ngOnInit() {}

  toPreviousPage() {
    this.location.back();
  }

}
