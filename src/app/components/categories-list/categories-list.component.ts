import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss'],
})
export class CategoriesListComponent implements OnInit {

  categories = [
    {
      title: 'Cat1',
      img: '../../assets/img/welcome.png'
    },
    {
      title: 'Cat2',
      img: '../../assets/img/usher.png'
    },
    {
      title: 'Cat3',
      img: '../../assets/img/usher.png'
    },
    {
      title: 'Cat4',
      img: '../../assets/img/welcome.png'
    },
    {
      title: 'Cat5',
      img: '../../assets/img/usher.png'
    },
    {
      title: 'Cat6',
      img: '../../assets/img/welcome.png'
    },
    {
      title: 'Cat7',
      img: '../../assets/img/usher.png'
    },
    {
      title: 'Cat8',
      img: '../../assets/img/welcome.png'
    }
  ];

  constructor(private router: Router) { }

  ngOnInit() {}

  navigateToItemsList(catName: string) {
    this.router.navigate(['page/items', catName.toLowerCase()]);
  }
}
