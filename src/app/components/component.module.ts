import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { SwiperModule } from 'swiper/angular';

import { BackToolbarComponent } from './back-toolbar/back-toolbar.component';
import { CategoriesListComponent } from './categories-list/categories-list.component';
import { InformationComponent } from './information/information.component';

@NgModule({
  declarations: [
    BackToolbarComponent,
    CategoriesListComponent,
    InformationComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    SwiperModule
  ],
  exports: [
    BackToolbarComponent,
    CategoriesListComponent,
    InformationComponent
  ]
})
export class ComponentModule { }
