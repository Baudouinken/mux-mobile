import { Person } from './Person';
import { Helper as helper } from '../shared/helper';

import { PERSONTYPE, ROLETYPE } from './Status';

export class Helper extends Person {
  type: PERSONTYPE;

  /**Creates an instance of the model */
  constructor(firstname: string, lastname: string, gender: string, email: string, phone: string) {
    super();
    this.id = null;
    this.firstname = firstname;
    this.lastname = lastname;
    this.gender = gender;
    this.email = email;
    this.phone = phone;
    this.role = ROLETYPE.helper;
  }

  public toString(): string {
    return `${this.firstname} ${this.lastname} - ${this.gender} (${this.email} - ${helper.capitalizeFirstLetter(this.role)})`;
  }
}
