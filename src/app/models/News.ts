import { AbstractBaseObject } from './AbstractBaseObject';

export class News extends AbstractBaseObject {
  id: string;
  title: string;
  description: string;
  image: File;
  createdBy: string;

  /**Creates an instance of this model */
  constructor(title: string, description: string, author: string) {
    super();
    this.id = null;
    this.title = title;
    this.description = description;
    this.createdBy = author;
  }

  /**
   * toString
   *
   * @returns string that describe this model
   */
   public toString(): string {
    return `Title: ${this.title} (Author: ${this.createdBy}`;
  }
}
