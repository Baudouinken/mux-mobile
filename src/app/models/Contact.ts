import { AbstractBaseObject } from './AbstractBaseObject';
import { STATUS } from './Status';

export class Contact extends AbstractBaseObject {
  id: string;
  subject: string;
  description: string;
  status: STATUS;

  /**Creates an instance of this model */
  constructor(description: string, subject: string = null, status?: STATUS) {
    super();
    this.id = null;
    this.description = description;
    this.subject = subject;
    this.status = status ?? STATUS.unread;
  }
}
