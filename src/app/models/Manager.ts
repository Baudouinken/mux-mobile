import { Person } from './Person';
import { ROLETYPE } from './Status';

export class Manager extends Person {
  /**Creates an instance of the model */
  constructor(firstname: string, lastname: string, gender: string, email: string, phone: string) {
    super();
    this.id = null;
    this.firstname = firstname;
    this.lastname = lastname;
    this.gender = gender;
    this.email = email;
    this.phone = phone;
    this.role = ROLETYPE.manager;
  }

}
