import { AbstractBaseObject } from './AbstractBaseObject';
import { ROLETYPE } from './Status';

import { Helper } from '../shared/helper';

export abstract class Person extends AbstractBaseObject {
  id: string;
  firstname: string;
  lastname: string;
  gender: string;
  nationality: string;
  dob: string;
  phone: string;
  email: string;
  address: string;
  password: string;
  role: ROLETYPE;

  /**
   * toString
   */
  public toString(): string {
    return `${this.firstname} ${this.lastname} - ${this.gender} (${this.email} - ${Helper.capitalizeFirstLetter(this.role)})`;
  }
}
