import { AbstractBaseObject } from './AbstractBaseObject';

export class Category extends AbstractBaseObject {
  id: string;
  title: string;
  image: string;
  createdBy: string;

  /**Creates an instance of this model */
  constructor(title: string, author: string) {
    super();
    this.id = null;
    this.createdBy = author;
    this.title = title;
  }

  /**
   * toString
   *
   * @returns string that describe this model
   */
  public toString(): string {
    return `Name: ${this.title} (Author: ${this.createdBy}`;
  }
}
