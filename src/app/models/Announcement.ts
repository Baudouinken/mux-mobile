import { AbstractBaseObject } from './AbstractBaseObject';
import { ANNOUNCEMENTSTATUS } from './Status';
import { Helper } from '../shared/helper';

export class Announcement extends AbstractBaseObject {
  id: string;
  categoryId: string;
  //type: string;
  images: string[];
  title: string;
  description: string;
  /**standby by default */
  status: ANNOUNCEMENTSTATUS;
  createdBy: string;
  createdAt: string;

  /**Creates an instance of an Announcement */
  constructor(title: string, description: string, author: string, categoryId: string, status?: ANNOUNCEMENTSTATUS) {
    super();
    this.id = null;
    this.title = title;
    this.description = description;
    this.createdBy = author;
    this.categoryId = categoryId;
    this.status = status ?? ANNOUNCEMENTSTATUS.standby;
  }

  /**
   * toString
   *
   * @returns string that describe an announcement
   */
  public toString(): string {
    return `${this.title} - Author: ${this.createdBy} (${Helper.capitalizeFirstLetter(this.status)})`;
  }
}
