export abstract class AbstractBaseObject {
  createdAt: string;
  updatedAt: string;
}
