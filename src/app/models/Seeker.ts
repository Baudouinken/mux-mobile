import { Person } from './Person';
import { ROLETYPE } from './Status';

export class Seeker extends Person {
  /**Creates an instance of the model */
  constructor(firstname?: string, lastname?: string,
    dob?: string, nationality?: string, email?: string, address?: string, phone?: string) {
    super();
    this.id = null;
    this.firstname = firstname;
    this.lastname = lastname;
    this.dob = dob;
    this.nationality = nationality;
    this.email = email;
    this.address = address;
    this.phone = phone;
    this.role = ROLETYPE.seeker;
  }
}
