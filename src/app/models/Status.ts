export enum ANNOUNCEMENTSTATUS {
  standby = 'STANDBY',
  published = 'PUBLISHED',
  rejected = 'REJECTED',
  processed = 'PROCESSED'
}

export enum PERSONTYPE {
  private = 'PRIVATE',
  organization = 'ORGANIZATION'
}

export enum ROLETYPE {
  admin = 'ADMIN',
  helper = 'HELPER',
  manager = 'MAAGER',
  seeker = 'SEEKER'
}

export enum STATUS {
  read = 'READ',
  unread = 'UNREAD'
}

export enum GENDER {
  m = 'M', f = 'F', u = 'U'
}


