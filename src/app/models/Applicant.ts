import { AbstractBaseObject } from './AbstractBaseObject';
import { STATUS } from './Status';
import { Helper } from '../shared/helper';

export class Applicant extends AbstractBaseObject {
  id: string;
  announcementId: string;
  personId: string;
  status: STATUS;
  /**The following information will be asked, when the user is not connected */
  email?: string;
  details: string;
  phone: string;
  name: string;
  // this will be use for the push notification purpose. (Later)
  deviceId?: string;

  /**Creates an instance of this model */
  constructor(announcementId: string, phone: string, name: string, email: string, text: string) {
    super();
    this.id = null;
    this.name = name;
    this.phone = phone;
    this.email = email;
    this.announcementId = announcementId;
    this.details = text;
    this.status = STATUS.unread;
  }

  /**
   * toString
   *
   * @returns string that describe this model
   */
  public toString(): string {
    return `Applicant: ${this.name} (${this.phone} ${this.email})`
           + `- Announcement: ${this.announcementId} (${Helper.capitalizeFirstLetter(this.status)})`;
  }

  public getDetails(): string {
    return ``;
  }
}
