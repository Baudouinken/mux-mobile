import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { ToastController } from '@ionic/angular';

import { Announcement } from 'src/app/models/Announcement';
import { Applicant } from 'src/app/models/Applicant';

@Component({
  selector: 'app-apply',
  templateUrl: './apply.page.html',
  styleUrls: ['./apply.page.scss'],
})
export class ApplyPage implements OnInit {

  public smallScreen = false;
  title: string;
  data: Announcement | any = {};
  german = '015223433333';
  ukraine = '+380914817994';
  inputTyePhone: FormControl;
  public success = false;
  public outgoing = {
    createdby: '',
    phone: '',
    item: ''
  };

  form = new FormGroup({
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    phone: new FormControl('', [Validators.required]),
    message: new FormControl('', [Validators.required]),
  });

  constructor(
    private router: Router,
    public toastController: ToastController
  ) {
    this.title = 'Message';
    this.data.img = '../../assets/img/welcome.png';
    this.data.title = 'article\'s name';
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.smallScreen = window.innerWidth < 768;
  }

  ngOnInit() {
    // get the device size
    this.smallScreen = window.innerWidth < 768;
  }

  checkRequiredFields() {
    if (this.form.get('name').invalid) {
      this.form.get('name').markAsTouched({ onlySelf: true });
    }

    if (this.form.get('email').invalid) {
      this.form.get('email').markAsTouched({ onlySelf: true });
    }

    if (this.form.get('phone').invalid) {
      this.form.get('phone').markAsTouched({ onlySelf: true });
    }

    if (this.form.get('message').invalid) {
      this.form.get('message').markAsTouched({ onlySelf: true });
    }
  }

  createApplication() {
    // Validate fields
    this.checkRequiredFields();
    const name = this.form.value.name;
    const email = this.form.value.email;
    const phone = this.form.value.phone;
    const text = this.form.value.message;

    const application = new Applicant('1', phone, name, email, text);
    // if success
    this.presentToastWithOptions();
    this.success = true;
    this.outgoing.createdby = `${name} (${email})`;
    this.outgoing.phone = phone;
    this.outgoing.item = this.data.title;
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      color: 'success',
      cssClass: 'text-white font-size-800',
      message: 'Your message has been sent successfully.',
      animated: true,
      position: 'bottom',
      translucent: false,
      icon: 'checkmark-circle-outline',
      duration: 2000
    });
    await toast.present();
  }
}
