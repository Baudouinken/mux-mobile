import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import SwiperCore, { Autoplay, Keyboard, Pagination, Scrollbar, Zoom } from 'swiper';

SwiperCore.use([Autoplay, Keyboard, Pagination, Scrollbar, Zoom]);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  public headerTitle: string;
  public images = [
    {
      src: '../../assets/img/usher.png',
      name: 'welcome1_img'
    },
    {
      src: '../../assets/img/welcome.png',
      name: 'welcome2_img'
    }
  ];

  categories = [
    {
      title: 'Cat1',
      img: '../../assets/img/welcome.png'
    },
    {
      title: 'Cat2',
      img: '../../assets/img/usher.png'
    },
    {
      title: 'Cat3',
      img: '../../assets/img/usher.png'
    },
    {
      title: 'Cat4',
      img: '../../assets/img/welcome.png'
    },
    {
      title: 'Cat5',
      img: '../../assets/img/usher.png'
    },
    {
      title: 'Cat6',
      img: '../../assets/img/welcome.png'
    },
    {
      title: 'Cat7',
      img: '../../assets/img/usher.png'
    },
    {
      title: 'Cat8',
      img: '../../assets/img/welcome.png'
    }
  ];

  constructor(private router: Router) {
    // iff user is connected and it is an adminitrator, the title has to be changed to Dashboard
    this.headerTitle = 'Home';
  }

  ngOnInit() {
  }

  navigateToItemsList(catName: string) {
    this.router.navigate(['page/items', catName.toLowerCase()]);
  }

}
