import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Category } from 'src/app/models/Category';

interface CategoryList {
  img: string;
  title: string;
}

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {

  public smallScreen = false;
  categories = [
    {
      title: 'Cat1',
      img: '../../assets/img/welcome.png'
    },
    {
      title: 'Cat2',
      img: '../../assets/img/usher.png'
    },
    {
      title: 'Cat3',
      img: '../../assets/img/usher.png'
    },
    {
      title: 'Cat4',
      img: '../../assets/img/welcome.png'
    },
    {
      title: 'Cat5',
      img: '../../assets/img/usher.png'
    },
    {
      title: 'Cat6',
      img: '../../assets/img/welcome.png'
    },
    {
      title: 'Cat7',
      img: '../../assets/img/usher.png'
    },
    {
      title: 'Cat8',
      img: '../../assets/img/welcome.png'
    }
  ];

  data: CategoryList[];
  public title = 'Categories';

  constructor(private router: Router) { }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.smallScreen = window.innerWidth < 768;
  }

  ngOnInit() {
    this.smallScreen = window.innerWidth < 768;
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.data = [];
      this.categories.forEach(x => this.data.push(x));
    }, 2000);
  }

  navigateToItemsList(catName: string) {
    this.router.navigate(['page/items', catName.toLowerCase()]);
  }
}
