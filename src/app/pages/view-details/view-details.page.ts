import { Location } from '@angular/common';
import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Announcement } from 'src/app/models/Announcement';

@Component({
  selector: 'app-view-details',
  templateUrl: './view-details.page.html',
  styleUrls: ['./view-details.page.scss'],
})
export class ViewDetailsPage implements OnInit {

  public data: Announcement | any = {};
  public smallScreen = false;

  constructor(
    private router: Router,
    private location: Location
  ) {
    this.data.id = 1;
    this.data.title = 'T-Shirt no make';
    this.data.description = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr,' +
                            'sed diam nonumy eirmod tempor invidunt ut labore et dolore magna';
    this.data.images = [
      '../../assets/img/usher.png',
      '../../assets/img/welcome.png'
    ];
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.smallScreen = window.innerWidth < 768;
  }

  ngOnInit() {
    // get the device size
    this.smallScreen = window.innerWidth < 768;
  }

  toApplicationPage() {
    const title = this.data.title.toString().split(' ').join('_').toString();
    this.router.navigate([`page/items/${title}/${this.data.id}/apply`]);
  }
}
