import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'page',
    component: TabsPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../dashboard/dashboard.module').then(m => m.DashboardPageModule)
      },
      {
        path: 'news',
        loadChildren: () => import('../news/news.module').then(m => m.NewsPageModule)
      },
      {
        path: 'about',
        loadChildren: () => import('../aboutus/aboutus.module').then(m => m.AboutusPageModule)
      },
      {
        path: 'items/:catName',
        loadChildren: () => import('../announcement/announcement.module').then(m => m.AnnouncementPageModule)
      },
      {
        path: 'items/:catName/:id',
        loadChildren: () => import('../view-details/view-details.module').then(m => m.ViewDetailsPageModule)
      },
      {
        path: 'items/:catName/:id/apply',
        loadChildren: () => import('../apply/apply.module').then(m => m.ApplyPageModule)
      },
      {
        path: 'category',
        loadChildren: () => import('../category/category.module').then(m => m.CategoryPageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo: 'page/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule { }
