import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Helper } from 'src/app/shared/helper';


interface AnnouncementList {
  id: string;
  img: string;
  title: string;
  desc: string;
}

@Component({
  selector: 'app-announcement',
  templateUrl: './announcement.page.html',
  styleUrls: ['./announcement.page.scss'],
})
export class AnnouncementPage implements OnInit {

  public categoryName: string;

  data: AnnouncementList[];

  anouncements: any = []; //Anouncement

  constructor(
    private activatedroute: ActivatedRoute,
    private router: Router
  ) {
    // Get the name of the category from the params
    this.activatedroute.paramMap.subscribe(params => this.categoryName = params.get('catName').toUpperCase());
  }

  // TODO: add a method that we fetch the items of the category given in parameter,
  // if the category does not exists redirect user to homepage.
  ngOnInit() {
    this.getAnnouncement();
  }

  getAnnouncement() {
    this.anouncements.push(
      {
        id: 1,
        img: '../../../assets/img/usher.png',
        title: 'Name',
        desc: Helper.extractWord('Lorem ipsum dolor sit amet, consectetur', 5),
      },
      {
        id: 2,
        img: '../../../assets/img/usher.png',
        title: 'Name',
        desc: Helper.extractWord('Lorem ipsum dolor sit amet, consectetur', 5),
      },
      {
        id: 3,
        img: '../../../assets/img/usher.png',
        title: 'Name',
        desc: Helper.extractWord('Lorem ipsum dolor sit amet, consectetur', 5),
      },
      {
        id: 4,
        img: '../../../assets/img/usher.png',
        title: 'Name',
        desc: Helper.extractWord('Lorem ipsum dolor sit amet, consectetur', 5),
      }
    );
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.data = [];
      this.anouncements.forEach(x => this.data.push(x));
    }, 2000);
  }

  navigateToDetails(id: string) {
    this.router.navigate(['page/items/' + this.categoryName, id]);
  }
}
